# Charlotte's Portfolio!

---
### This portfolio demonstrates:

- My capability with web development
- Showcases several of my top projects and proudest
- Shows my competency to put together a project in a short period of time, and make it look pretty :)

The portfolio includes several links to my socials, email, resume, and of course top projects.

## More links!
[The Website itself](https://charlotte-2222.github.io/char-site/)
<br>
<p>
<a href="mailto:ayy.charlotte@gmail.com">Email me!</a><br>

<a href="https://discord.gg/xspn5FpfJM">Contact Discord</a>
</p>